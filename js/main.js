const an1 = document.querySelector('#goat');
const an2 = document.querySelector('#donkey');
const an3 = document.querySelector('#cow');
const an4 = document.querySelector('#pig');
const an5 = document.querySelector('#horse');
const an6 = document.querySelector('#chicken');
const an7 = document.querySelector('#sheep');

const pb1 = document.querySelector('#plbtn1');
const pb2 = document.querySelector('#plbtn2');
const pb3 = document.querySelector('#plbtn3');
const pb4 = document.querySelector('#plbtn4');
const pb5 = document.querySelector('#plbtn5');
const pb6 = document.querySelector('#plbtn6');
const pb7 = document.querySelector('#plbtn7');
const pb = [pb1, pb2, pb3, pb4, pb5, pb6, pb7];

const bl1 = document.querySelector('#anim1');
const bl2 = document.querySelector('#anim2');
const bl3 = document.querySelector('#anim3');
const bl4 = document.querySelector('#anim4');
const bl5 = document.querySelector('#anim5');
const bl6 = document.querySelector('#anim6');
const bl7 = document.querySelector('#anim7');
// const bls = [bl1, bl2, bl3, bl4, bl5, bl6, bl7];

const reset = document.querySelector('.reset');

var anim1 = {
    clicked: false,
    pb: document.querySelector('#plbtn1'),
    img: an1,
    snd: new Audio('../FarmSampler/assets/snd/goat.wav'),
};

var anim2 = {
    clicked: false,
    pb: document.querySelector('#plbtn2'),
    img: an2,
    snd: new Audio('../FarmSampler/assets/snd/donkey.wav'),
};

var anim3 = {
    clicked: false,
    pb: document.querySelector('#plbtn3'),
    img: an3,
    snd: new Audio('../FarmSampler/assets/snd/cow.wav'),
};

var anim4 = {
    clicked: false,
    pb: document.querySelector('#plbtn4'),
    img: an4,
    snd: new Audio('../FarmSampler/assets/snd/pig.wav'),
};

var anim5 = {
    clicked: false,
    pb: document.querySelector('#plbtn5'),
    img: an5,
    snd: new Audio('../FarmSampler/assets/snd/horse.wav'),
};

var anim6 = {
    clicked: false,
    pb: document.querySelector('#plbtn6'),
    img: an6,
    snd: new Audio('../FarmSampler/assets/snd/chicken.wav'),
};

var anim7 = {
    clicked: false,
    pb: document.querySelector('#plbtn7'),
    img: an7,
    snd: new Audio('../FarmSampler/assets/snd/sheep.wav'),
};


const anim = [anim1, anim2, anim3, anim4, anim5, anim6, anim7];
for (var i = 0; i < 7; i++) {
    anim[i].snd.volume = 0.3
}

// This piece of code made me angry because it didn't want to work initially
// but now I didn't care enough to use it.

// function logArrayElements(element, index, array) {
//     // console.log(index);
//     return (index);
// }

// pb.forEach(logArrayElements);

// for (var bl of bls) {
//     bl.addEventListener('mouseover', function() {
//         for (let i = 0; i < 6; i++) {
//             if (bl.forEach(logArrayElements) == pb.forEach(logArrayElements)) {
//                 pb[i].style.display = "block";
//             }
            
//         }
//     })
//     an.addEventListener('mouseout', function() {
//         for (let i = 0; i < 6; i++) {
//             if (an.forEach(logArrayElements) == pb.forEach(logArrayElements)) {
//                 pb[i].style.display = "none"
//             }
//         }
//     })
// }

// In any case, I hate to do this, but...

bl1.addEventListener('mouseenter', function() {
    pb1.style.display = "block";
});
bl1.addEventListener('mouseleave', function(){
    pb1.style.display = "none";
});

bl2.addEventListener('mouseenter', function() {
    pb2.style.display = "block";
});
bl2.addEventListener('mouseleave', function(){
    pb2.style.display = "none";
});

bl3.addEventListener('mouseenter', function() {
    pb3.style.display = "block";
});
bl3.addEventListener('mouseleave', function(){
    pb3.style.display = "none";
});

bl4.addEventListener('mouseenter', function() {
    pb4.style.display = "block";
});
bl4.addEventListener('mouseleave', function(){
    pb4.style.display = "none";
});

bl5.addEventListener('mouseenter', function() {
    pb5.style.display = "block";
});
bl5.addEventListener('mouseleave', function(){
    pb5.style.display = "none";
});

bl6.addEventListener('mouseenter', function() {
    pb6.style.display = "block";
});
bl6.addEventListener('mouseleave', function(){
    pb6.style.display = "none";
});

bl7.addEventListener('mouseenter', function() {
    pb7.style.display = "block";
});
bl7.addEventListener('mouseleave', function(){
    pb7.style.display = "none";
});

anim1.pb.addEventListener('click', function(){
    if (anim1.clicked == false) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/stop.png')";
        anim1.clicked = true;
        anim1.img.src = "../FarmSampler/assets/img/anim/goat_part2.gif";
        anim1.snd.loop = true;
        anim1.snd.play();
    }
    else if (anim1.clicked == true) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/play.png')";
        anim1.clicked = false;
        anim1.img.src = "../FarmSampler/assets/img/anim/goat_part1.gif";
        anim1.snd.pause() ;
    }
});

anim2.pb.addEventListener('click', function(){
    if (anim2.clicked == false) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/stop.png')";
        anim2.clicked = true;
        anim2.img.src = "../FarmSampler/assets/img/anim/donkey_part2.gif";
        anim2.snd.loop = true;
        anim2.snd.play();
    }
    else if (anim2.clicked == true) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/play.png')";
        anim2.clicked = false;
        anim2.img.src = "../FarmSampler/assets/img/anim/donkey_part1.gif";
        anim2.snd.pause();
    }
});

anim3.pb.addEventListener('click', function(){
    if (anim3.clicked == false) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/stop.png')";
        anim3.clicked = true;
        anim3.img.src = "../FarmSampler/assets/img/anim/cow_part2.gif";
        anim3.snd.loop = true;
        anim3.snd.play();
    }
    else if (anim3.clicked == true) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/play.png')";
        anim3.clicked = false;
        anim3.img.src = "../FarmSampler/assets/img/anim/cow_part1.gif";
        anim3.snd.pause();
    }
});

anim4.pb.addEventListener('click', function(){
    if (anim4.clicked == false) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/stop.png')";
        anim4.clicked = true;
        anim4.img.src = "../FarmSampler/assets/img/anim/pig_part2.gif";
        anim4.snd.loop = true;
        anim4.snd.play();
    }
    else if (anim4.clicked == true) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/play.png')"
        anim4.clicked = false;
        anim4.img.src = "../FarmSampler/assets/img/anim/pig_part1.gif";
        anim4.snd.pause();
    }
});

anim5.pb.addEventListener('click', function(){
    if (anim5.clicked == false) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/stop.png')";
        anim5.clicked = true;
        anim5.img.src = "../FarmSampler/assets/img/anim/horse_part2.gif";
        anim5.snd.loop = true;
        anim5.snd.play();
    }
    else if (anim5.clicked == true) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/play.png')"
        anim5.clicked = false;
        anim5.img.src = "../FarmSampler/assets/img/anim/horse_part1.gif";
        anim5.snd.pause();
    }
});

anim6.pb.addEventListener('click', function(){
    if (anim6.clicked == false) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/stop.png')";
        anim6.clicked = true;
        anim6.img.src = "../FarmSampler/assets/img/anim/chicken_part2.gif";
        anim6.snd.loop = true;
        anim6.snd.play();
    }
    else if (anim6.clicked == true) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/play.png')";
        anim6.clicked = false;
        anim6.img.src = "../FarmSampler/assets/img/anim/chicken_part1.gif";
        anim6.snd.pause();
    }
});

anim7.pb.addEventListener('click', function(){
    if (anim7.clicked == false) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/stop.png')";
        anim7.clicked = true;
        anim7.img.src = "../FarmSampler/assets/img/anim/sheep_part2.gif";
        anim7.snd.loop = true;
        anim7.snd.play();
    }
    else if (anim7.clicked == true) {
        this.style.backgroundImage = "url('../FarmSampler/assets/img/play.png')"
        anim7.clicked = false;
        anim7.img.src = "../FarmSampler/assets/img/anim/sheep_part1.gif";
        anim7.snd.pause();
    }
});

reset.addEventListener('click', function(){
    for (var i=0; i < 7; i++) {
        pb[i].style.backgroundImage = "url('../FarmSampler/assets/img/play.png')";
        anim[i].snd.pause();
    }
    anim1.img.src = "../FarmSampler/assets/img/anim/goat_part1.gif";
    anim2.img.src = "../FarmSampler/assets/img/anim/donkey_part1.gif";
    anim3.img.src = "../FarmSampler/assets/img/anim/cow_part1.gif";
    anim4.img.src = "../FarmSampler/assets/img/anim/pig_part1.gif";
    anim5.img.src = "../FarmSampler/assets/img/anim/horse_part1.gif";
    anim6.img.src = "../FarmSampler/assets/img/anim/chicken_part1.gif";
    anim7.img.src = "../FarmSampler/assets/img/anim/sheep_part1.gif";
});